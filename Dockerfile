FROM alpine:latest
RUN mkdir /app
COPY . /app
RUN apk update \
	&& apk upgrade \
	&& apk add darkhttpd
EXPOSE 8000/tcp
CMD ["darkhttpd", "/app", "--port", "8000"]

