# [onosm.osmiran.ir](https://onosm.osmiran.ir/)

یک راه آسان برای وارد کردن کسب و کار خود به نقشه OpenStreetMap


## نحوه مشارکت در پروژه
 * برای اضافه کردن موارد جدید به دسته بندی ها فایل categories.json را در /locales/fa-IR طبق الگو ویرایش کنید.
 * Site specific css lives in ```css/site.css``` for positioning of specific elements
 * Global style changes live in ```css/style.css```
 * Translations live in ```locales``` and are more than welcome!
 * Prefer 4 space soft tab in javascript
 * For UI changes, don't forget to include a screenshot or two in the pull request
 

#### سورس اصلی پروژه
بر اساس [onosm.org](https://github.com/osmlab/onosm.org)